package testdata

import (
	"bufio"
	"context"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/sdk/trace/tracetest"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
)

var traceMu sync.Mutex

func AssertAttribute[T any](
	tb testing.TB,
	s attribute.Set,
	key attribute.Key,
	expectedType attribute.Type,
	getter func(attribute.Value) T,
	expected T,
) {
	tb.Helper()

	if value, ok := s.Value(key); assert.True(tb, ok) {
		if assert.Equal(tb, expectedType, value.Type()) {
			actual := getter(value)

			assert.Equal(tb, expected, actual, "%q value", key)
		}
	}

	return
}

func AssertCodeAttributes(
	tb testing.TB,
	attrs attribute.Set,
	namespace,
	function,
	file string,
	line int64,
) {
	tb.Helper()

	wd, err := os.Getwd()
	require.NoError(tb, err, "get working directory")

	AssertAttribute(
		tb,
		attrs,
		semconv.CodeNamespaceKey,
		attribute.STRING,
		attribute.Value.AsString,
		namespace,
	)

	AssertAttribute(
		tb,
		attrs,
		semconv.CodeFunctionKey,
		attribute.STRING,
		attribute.Value.AsString,
		function,
	)

	AssertAttribute(
		tb,
		attrs,
		semconv.CodeFilepathKey,
		attribute.STRING,
		attribute.Value.AsString,
		filepath.Join(wd, file),
	)

	AssertAttribute(
		tb,
		attrs,
		semconv.CodeLineNumberKey,
		attribute.INT64,
		attribute.Value.AsInt64,
		line,
	)
}

func LineNumber(tb testing.TB, pattern string) int64 {
	tb.Helper()

	re, err := regexp.Compile(pattern)
	require.NoError(tb, err, "compile regex")

	_, file, _, ok := runtime.Caller(1)
	require.True(tb, ok, "get caller")

	f, err := os.Open(file)
	require.NoError(tb, err, "open file")

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for i := int64(1); scanner.Scan(); i++ {
		if re.MatchString(scanner.Text()) {
			return i
		}
	}

	require.NoError(tb, scanner.Err(), "no scanner error")
	require.Fail(tb, "", "no match for %q", pattern)

	return -1
}

func Spans(
	ctx context.Context,
	tb testing.TB,
	𝑓 func(context.Context),
) []tracetest.SpanStub {
	traceMu.Lock()
	defer traceMu.Unlock()

	exporter := tracetest.NewInMemoryExporter()

	tracingSpanProcessor := sdktrace.NewBatchSpanProcessor(exporter)
	tb.Cleanup(func() {
		tracingSpanProcessor.Shutdown(ctx)
	})

	tracingProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSpanProcessor(tracingSpanProcessor),
	)
	tb.Cleanup(func() {
		tracingProvider.Shutdown(ctx)
	})

	otel.SetTracerProvider(tracingProvider)

	𝑓(ctx)

	tracingSpanProcessor.ForceFlush(ctx)

	return exporter.GetSpans()
}
