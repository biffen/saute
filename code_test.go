package sauté_test

import (
	"testing"

	sauté "gitlab.com/biffen/saute"
	"gitlab.com/biffen/saute/testdata"
	"go.opentelemetry.io/otel/attribute"
)

var noFuncAttrs = sauté.CodeAttributes()

func TestCodeAttributes(t *testing.T) {
	t.Parallel()

	attrs := sauté.CodeAttributes() // *1*

	t.Run("test function", func(t *testing.T) {
		t.Parallel()

		testdata.AssertCodeAttributes(
			t,
			attribute.NewSet(attrs...),
			"gitlab.com/biffen/saute_test",
			"TestCodeAttributes",
			"code_test.go",
			testdata.LineNumber(t, `\*1\*`),
		)
	})

	t.Run("anonymous function", func(t *testing.T) {
		t.Parallel()

		attrs := sauté.CodeAttributes() // *2*

		testdata.AssertCodeAttributes(
			t,
			attribute.NewSet(attrs...),
			"gitlab.com/biffen/saute_test.TestCodeAttributes",
			"func2",
			"code_test.go",
			testdata.LineNumber(t, `\*2\*`),
		)
	})

	t.Run("no function", func(t *testing.T) {
		t.Parallel()

		testdata.AssertCodeAttributes(
			t,
			attribute.NewSet(noFuncAttrs...),
			"gitlab.com/biffen/saute_test",
			"init",
			"code_test.go",
			testdata.LineNumber(t, `^var noFuncAttrs\b`),
		)
	})
}
