# 🍳 Super-Automatic Telemetry

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/biffen/saute.svg)](https://pkg.go.dev/gitlab.com/biffen/saute)
![Licence](https://img.shields.io/gitlab/license/biffen%2Fsaute?style=flat-square)
![GitLab Release (by release name)](https://img.shields.io/gitlab/v/release/biffen%2Fsaute?sort=semver&style=flat-square&link=https%3A%2F%2Fgitlab.com%2Fbiffen%2Fsaute%2F-%2Freleases)

---

Sauté makes it super easy to set up [OpenTelemetry] in [Go].

With a single function call both metrics and tracing are set up based on
environment variables.

-   [autoexport] to do most of the setup
-   [host] for host metrics
-   [runtime] for runtime metrics
-   [autoprop] to set propagators
-   Telemetry logging is redirected to [slog]

## Configuration

Most of the configuration is through environment variables, as per the
[Environment Variable Specification] (see also the [OpenTelemetry Protocol
Exporter] documentation), but some things can also be set with options to
[`sauté.Init()`].

## Example

```go
import sauté "gitlab.com/biffen/saute"

func main() {
	// Set up context, logging, etc.

	fini, _ := sauté.Init(ctx)
	defer fini()

	// The rest of your application goes here.
}
```

## Licence

[MIT][LICENSE.txt]

[Environment Variable Specification]: https://opentelemetry.io/docs/specs/otel/configuration/sdk-environment-variables/
[Go]: https://go.dev
[LICENSE.txt]: ./LICENSE.txt
[OpenTelemetry Protocol Exporter]: https://opentelemetry.io/docs/specs/otel/protocol/exporter/
[OpenTelemetry]: https://opentelemetry.io
[`sauté.Init()`]: https://pkg.go.dev/gitlab.com/biffen/saute#Init
[autoexport]: https://pkg.go.dev/go.opentelemetry.io/contrib/exporters/autoexport
[autoprop]: https://pkg.go.dev/go.opentelemetry.io/contrib/propagators/autoprop
[host]: https://pkg.go.dev/go.opentelemetry.io/contrib/instrumentation/host
[runtime]: https://pkg.go.dev/go.opentelemetry.io/contrib/instrumentation/runtime
[slog]: https://pkg.go.dev/log/slog
