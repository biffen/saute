.POSIX:
.SUFFIXES:

SHELL                           = /bin/sh

GOLANGCI_LINT_FLAGS            ?= --verbose
GOLANGCI_LINT_LINTERS          ?= asasalint,bidichk,bodyclose,containedctx,contextcheck,cyclop,decorder,dogsled,dupl,dupword,durationcheck,errcheck,errchkjson,errname,errorlint,execinquery,exhaustive,exportloopref,forbidigo,forcetypeassert,gci,gocheckcompilerdirectives,gocognit,goconst,gocritic,gocyclo,godot,godox,goerr113,gofmt,gofumpt,goheader,goimports,gomnd,gomoddirectives,gomodguard,goprintffuncname,gosec,gosimple,govet,grouper,importas,ineffassign,interfacebloat,lll,loggercheck,maintidx,makezero,mirror,misspell,musttag,nestif,nilerr,nilnil,noctx,nolintlint,nosprintfhostport,paralleltest,perfsprint,prealloc,predeclared,promlinter,reassign,revive,rowserrcheck,sloglint,sqlclosecheck,staticcheck,stylecheck,tagalign,tagliatelle,tenv,testableexamples,testpackage,thelper,tparallel,typecheck,unconvert,unparam,unused,usestdlibvars,wastedassign,whitespace,wrapcheck
GOLANGCI_LINT_RUN_FLAGS        ?= --disable-all --enable=$(GOLANGCI_LINT_LINTERS) --max-issues-per-linter 1024 --max-same-issues 1024 --no-config --sort-results --timeout=1h
GO_MODULE                      != go list
GO_TEST_FLAGS                  ?= -covermode atomic -coverpkg $(GO_MODULE)/... -coverprofile coverage.txt -race -v

all: check

check: lint test

clean: mostlyclean

coverage: coverage.txt
	go tool cover -func $<

fix:
	golangci-lint $(GOLANGCI_LINT_FLAGS) run $(GOLANGCI_LINT_RUN_FLAGS) --fix

golangci-lint:
	golangci-lint $(GOLANGCI_LINT_FLAGS) run $(GOLANGCI_LINT_RUN_FLAGS)

lint: golangci-lint

mostlyclean:
	rm -f \
		*codeclimate.json \
		*junit.xml \
		coverage.* \
		test.out

test:
	go test $(GO_TEST_FLAGS) ./...

.PHONY: all check clean coverage fix golangci-lint lint mostlyclean test

coverage.txt: test

coverage.html: coverage.txt
	go tool cover -html $< -o $@

coverate.xml: coverage.txt
	gocover-cobertura < $< > $@

golangci-lint.codeclimate.json:
	golangci-lint $(GOLANGCI_LINT_FLAGS) run $(GOLANGCI_LINT_RUN_FLAGS) --out-format code-climate >$@

gotest.junit.xml: test.out
	go-junit-report -set-exit-code < $< > $@

test.out:
	go test $(GO_TEST_FLAGS) ./... | tee $@
