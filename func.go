package sauté

import (
	"context"

	"gitlab.com/biffen/saute/internal"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
)

// TraceFunc returns a new [go.opentelemetry.io/otel/trace.Span] for the calling
// function.
//
// An optional [go.opentelemetry.io/otel/trace.Tracer] can be passed. If it is
// nil a default Tracer is used.
//
// # Example
//
//	package mypackage
//
//	func MyFunction(ctx context.Context) {
//	  ctx, span := saute.TraceFunc(ctx, nil)
//	  defer span.End()
//
//	  // The span is named something like
//	  // ‘example.example/mymodule/mypackage.MyFunction’ and has attributes for
//	  // file, line, function, etc.
//	}
func TraceFunc(
	ctx context.Context,
	tracer trace.Tracer,
	opts ...trace.SpanStartOption,
) (context.Context, trace.Span) {
	attrs, function := internal.Code(1)

	if tracer == nil {
		tracer = otel.Tracer("")
	}

	return tracer.Start(ctx, function, append(opts, trace.WithAttributes(attrs...))...)
}
