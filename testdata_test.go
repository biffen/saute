package sauté_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/saute/testdata"
)

func TestLineNumber(t *testing.T) {
	t.Parallel()

	assert.EqualValues(
		t,
		1,
		testdata.LineNumber(t, `^package\b`),
		"‘package’ on line 1",
	)
}
