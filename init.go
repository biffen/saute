package sauté

import (
	"context"

	"gitlab.com/biffen/saute/internal"
)

// Init is the main function of sauté. It starts telemetry and returns a
// function to stop it.
//
// If things go really bad it returns an error. The stop function is never nil,
// but it might be a no-op.
//
// Most things are configured with environment variables, see OpenTelemetry’s
// [Environment Variable Specification]. [Option]:s can be used to override some
// things.
//
// [Environment Variable Specification]: https://opentelemetry.io/docs/specs/otel/configuration/sdk-environment-variables/
//
//nolint:lll
func Init(
	ctx context.Context,
	options ...Option,
) (stop func(), err error) {
	stop = func() {}

	config := internal.NewConfig(ctx)

	for _, o := range options {
		if err = o.Apply(ctx, config); err != nil {
			return
		}
	}

	var s *internal.Sauté
	s, err = internal.New(ctx, config)
	if err != nil || s == nil {
		return
	}

	return func() {
		s.Stop(context.Background())
	}, nil
}
