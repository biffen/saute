# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic
Versioning].

## v0.0.3 - 2024-04-11

### Added

-   `TraceFunc`
-   `CodeAttributes`

## v0.0.2 - 2024-03-25

### Added

-   `ErrorHandler` option

## v0.0.1 - 2023-11-05

Initial release.

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
