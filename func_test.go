package sauté_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	sauté "gitlab.com/biffen/saute"
	"gitlab.com/biffen/saute/testdata"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

func Function(ctx context.Context) {
	_, span := sauté.TraceFunc(ctx, nil) // {Function}
	defer span.End()
}

func TestTraceFunc(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	t.Run("function", func(t *testing.T) {
		t.Parallel()

		spans := testdata.Spans(ctx, t, Function)

		require.Len(t, spans, 1, "1 span")

		span := spans[0]

		assert.Equal(
			t,
			"gitlab.com/biffen/saute_test.Function",
			span.Name,
			"span name",
		)

		attrs := attribute.NewSet(span.Attributes...)

		testdata.AssertCodeAttributes(
			t,
			attrs,
			"gitlab.com/biffen/saute_test",
			"Function",
			"func_test.go",
			testdata.LineNumber(t, `\{Function\}`),
		)
	})

	t.Run("method", func(t *testing.T) {
		t.Parallel()

		var v Type

		spans := testdata.Spans(ctx, t, v.Method)

		require.Len(t, spans, 1, "1 span")

		span := spans[0]

		assert.Equal(
			t,
			"gitlab.com/biffen/saute_test.Type.Method",
			span.Name,
			"span name",
		)

		attrs := attribute.NewSet(span.Attributes...)

		testdata.AssertCodeAttributes(
			t,
			attrs,
			"gitlab.com/biffen/saute_test.Type",
			"Method",
			"func_test.go",
			testdata.LineNumber(t, `\{Method\}`),
		)
	})
}

type Type struct{}

func (Type) Method(ctx context.Context) {
	_, span := sauté.TraceFunc(ctx, nil, trace.WithAttributes( // {Method}
		attribute.String("test", "räksmörgås"),
	))
	defer span.End()
}
