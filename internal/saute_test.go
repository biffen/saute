package internal_test

import (
	"context"
	"log/slog"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	sauté "gitlab.com/biffen/saute"
	"gitlab.com/biffen/saute/internal"
	"go.opentelemetry.io/otel"
)

var _ otel.ErrorHandler = (*MockErrorHandler)(nil)

//nolint:paralleltest // Globals
func TestNew(t *testing.T) {
	ctx := context.Background()

	t.Run("disabled", func(t *testing.T) {
		t.Setenv("OTEL_SDK_DISABLED", "true")

		s, err := internal.New(ctx, internal.NewConfig(ctx))
		assert.Nil(t, s, "no Sauté")
		assert.NoError(t, err, "no error either")
	})

	t.Run("custom logger", func(t *testing.T) {
		var (
			c      = internal.NewConfig(ctx)
			called atomic.Bool
			h      Handler = func(context.Context, slog.Record) error {
				called.Store(true)
				return nil
			}
		)

		err := sauté.Logger(slog.New(h)).Apply(ctx, c)
		require.NoError(t, err, "apply Logger option")

		s, err := internal.New(ctx, c)
		require.NoError(t, err, "no error creating Sauté")
		require.NotNil(t, s, "a Sauté")

		s.Stop(ctx)

		assert.True(t, called.Load(), "custom logger called")
	})

	t.Run("no logger", func(t *testing.T) {
		var (
			c      = internal.NewConfig(ctx)
			called atomic.Bool
			h      Handler = func(_ context.Context, record slog.Record) error {
				t.Log(record.Message)
				called.Store(true)
				return nil
			}
		)

		slog.SetDefault(slog.New(h))

		err := sauté.Logger(nil).Apply(ctx, c)
		require.NoError(t, err, "apply Logger option")

		s, err := internal.New(ctx, c)
		require.NoError(t, err, "no error creating Sauté")
		require.NotNil(t, s, "a Sauté")

		s.Stop(ctx)

		assert.False(t, called.Load(), "default logger not called")
	})
}

func TestSauté_SetErrorHandler(t *testing.T) {
	t.Parallel()

	var (
		actual otel.ErrorHandler
		config internal.Config
		s      internal.Sauté
	)

	𝑓 := func(tb testing.TB, expected otel.ErrorHandler, msg string) {
		tb.Helper()

		actual = nil

		s.SetErrorHandler(&config, func(handler otel.ErrorHandler) {
			actual = handler
		})

		assert.Equal(tb, expected, actual, msg)
	}

	var (
		ctx  = context.Background()
		mock MockErrorHandler
	)

	𝑓(t, &s, "nothing → default")

	err := sauté.ErrorHandler(nil).Apply(ctx, &config)
	assert.NoError(t, err, "ErrorHandler(nil)")

	𝑓(t, nil, "nil → none")

	err = sauté.ErrorHandler(mock).Apply(ctx, &config)
	assert.NoError(t, err, "ErrorHandler(mock)")

	𝑓(t, mock, "specified → that")
}

type MockErrorHandler struct{}

func (MockErrorHandler) Handle(error) {}
