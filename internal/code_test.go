package internal_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/saute/internal"
	"gitlab.com/biffen/saute/testdata"
	"go.opentelemetry.io/otel/attribute"
)

func TestCode(t *testing.T) { // HERE
	t.Parallel()

	attrs, function := internal.Code(0)

	assert.Equal(
		t,
		"gitlab.com/biffen/saute/internal_test.TestCode",
		function,
		"function name",
	)

	actual := attribute.NewSet(attrs...)

	testdata.AssertCodeAttributes(
		t,
		actual,
		"gitlab.com/biffen/saute/internal_test",
		"TestCode",
		"code_test.go",
		testdata.LineNumber(t, `internal\.Code`),
	)
}
