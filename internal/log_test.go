package internal_test

import (
	"context"
	"log/slog"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/saute/internal"
)

var (
	_             slog.Handler = (*Handler)(nil)
	logLevelTests              = map[string]slog.Level{
		"":              slog.LevelInfo,
		"ALL":           internal.LevelAll,
		"Alert":         slog.LevelError,
		"Critical":      slog.LevelError,
		"DEBUG":         slog.LevelDebug,
		"Debug":         slog.LevelDebug,
		"ERROR":         slog.LevelError,
		"Emergency":     slog.LevelError,
		"Error":         slog.LevelError,
		"INFO":          slog.LevelInfo,
		"Informational": slog.LevelInfo,
		"NONE":          internal.LevelNone,
		"Notice":        slog.LevelInfo,
		"VERBOSE":       internal.LevelAll,
		"WARN":          slog.LevelWarn,
		"Warning":       slog.LevelWarn,
		"alert":         slog.LevelError,
		"crit":          slog.LevelError,
		"critical":      slog.LevelError,
		"dbg":           slog.LevelDebug,
		"debug":         slog.LevelDebug,
		"emerg":         slog.LevelError,
		"emergency":     slog.LevelError,
		"err":           slog.LevelError,
		"error":         slog.LevelError,
		"fatal":         slog.LevelError,
		"info":          slog.LevelInfo,
		"notice":        slog.LevelInfo,
		"panic":         slog.LevelError,
		"warn":          slog.LevelWarn,
		"warning":       slog.LevelWarn,
	}
)

func FuzzParseLogLevel(f *testing.F) {
	ctx := context.Background()

	for str := range logLevelTests {
		f.Add(str)
	}

	f.Fuzz(func(t *testing.T, str string) {
		level, err := internal.ParseLogLevel(ctx, str)
		if err != nil {
			assert.ErrorIs(t, err, internal.ErrBadLogLevel, "the expected type of error")
		}

		assert.Contains(t, []slog.Level{
			internal.LevelAll,
			slog.LevelDebug,
			slog.LevelInfo,
			slog.LevelWarn,
			slog.LevelError,
			internal.LevelNone,
		}, level, "a known level")
	})
}

func TestHandler(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	𝑓 := func(t *testing.T, level slog.Level, expected uint64) {
		t.Helper()

		t.Run(level.String(), func(t *testing.T) {
			t.Helper()

			var (
				called atomic.Uint64
				h      Handler = func(context.Context, slog.Record) error {
					called.Add(1)
					return nil
				}
				handler = internal.Handler{
					Level: level,
					Next:  h,
				}
				s = slog.New(&handler)
			)

			s.DebugContext(ctx, t.Name())
			s.InfoContext(ctx, t.Name())
			s.WarnContext(ctx, t.Name())
			s.ErrorContext(ctx, t.Name())

			assert.Equal(t, expected, called.Load())
		})
	}

	𝑓(t, internal.LevelAll, 4)
	𝑓(t, slog.LevelDebug, 4)
	𝑓(t, slog.LevelInfo, 3)
	𝑓(t, slog.LevelWarn, 2)
	𝑓(t, slog.LevelError, 1)
	𝑓(t, internal.LevelNone, 0)
}

func TestParseLogLevel(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	𝑓 := func(t *testing.T, str string, expectedLevel slog.Level, expectedError error) {
		t.Helper()

		t.Run(str, func(t *testing.T) {
			t.Helper()

			level, err := internal.ParseLogLevel(ctx, str)
			if expectedError != nil {
				assert.ErrorIs(t, err, expectedError, "expected error")
				return
			}

			assert.NoError(t, err, "no error")
			assert.Equal(t, expectedLevel, level, "expected level")
		})
	}

	for str, expectedLevel := range logLevelTests {
		𝑓(t, str, expectedLevel, nil)
	}

	𝑓(t, " ", slog.LevelInfo, internal.ErrBadLogLevel)
	𝑓(t, "nope", slog.LevelInfo, internal.ErrBadLogLevel)
	𝑓(t, "💩", slog.LevelInfo, internal.ErrBadLogLevel)
}

type Handler func(context.Context, slog.Record) error

func (h Handler) Enabled(context.Context, slog.Level) bool {
	return true
}

func (h Handler) Handle(ctx context.Context, record slog.Record) error {
	return h(ctx, record)
}

func (h Handler) WithAttrs([]slog.Attr) slog.Handler {
	panic("not supported")
}

func (h Handler) WithGroup(string) slog.Handler {
	panic("not supported")
}
