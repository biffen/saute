package internal

import (
	"context"
	"log/slog"
	"os"

	"go.opentelemetry.io/contrib/propagators/autoprop"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
)

type Config struct {
	Attributes        []attribute.KeyValue
	Detecors          []resource.Detector
	ErrorHandler      *otel.ErrorHandler
	Getenv            func(string) string
	Host              bool
	LogLevel          *slog.Level
	Logger            *slog.Logger
	Resource          *resource.Resource
	Runtime           bool
	TextMapPropagator propagation.TextMapPropagator
}

func NewConfig(context.Context) *Config {
	return &Config{
		Getenv:            os.Getenv,
		Host:              true,
		Logger:            slog.Default().With(slog.String("subsystem", "telemetry")),
		Runtime:           true,
		TextMapPropagator: autoprop.NewTextMapPropagator(),
	}
}
