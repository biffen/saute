package internal

import (
	"context"
)

var _ Option = (*OptionFunc)(nil)

type Option interface {
	Apply(context.Context, *Config) error
}

type OptionFunc func(context.Context, *Config) error

func (𝑓 OptionFunc) Apply(ctx context.Context, config *Config) error {
	return 𝑓(ctx, config)
}
