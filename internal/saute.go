package internal

import (
	"context"
	"fmt"
	"log/slog"
	"slices"

	"github.com/go-logr/logr"
	"go.opentelemetry.io/contrib/exporters/autoexport"
	"go.opentelemetry.io/contrib/instrumentation/host"
	"go.opentelemetry.io/contrib/instrumentation/runtime"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
)

var _ otel.ErrorHandler = (*Sauté)(nil)

type NamedShutdowner struct {
	string
	Shutdowner
}

type Sauté struct {
	*slog.Logger
	shutdowners []NamedShutdowner
}

func New(ctx context.Context, config *Config) (s *Sauté, err error) {
	if disabled, err := OTEL_SDK_DISABLED.Get(ctx, config.Getenv); err != nil {
		return nil, err
	} else if disabled {
		//nolint:nilnil // On purpose.
		return nil, nil
	}

	s = new(Sauté)

	var level slog.Level
	if config.LogLevel == nil {
		if level, err = OTEL_LOG_LEVEL.Get(ctx, config.Getenv); err != nil {
			return nil, fmt.Errorf("error getting log level: %w", err)
		}
	} else {
		level = *config.LogLevel
	}

	handler := &Handler{
		Level: level,
	}

	if config.Logger != nil {
		handler.Next = config.Logger.Handler()
	}

	s.Logger = slog.New(handler)
	otel.SetLogger(logr.FromSlogHandler(handler))

	s.SetErrorHandler(config, otel.SetErrorHandler)

	res, err := s.resource(ctx, config)
	if err != nil {
		return nil, fmt.Errorf("error while constructing resources: %w", err)
	}

	if err := s.metrics(ctx, config, res); err != nil {
		s.WarnContext(ctx, "error setting up metrics",
			slog.Any("error", err),
		)
	}

	if err := s.tracing(ctx, config, res); err != nil {
		s.WarnContext(ctx, "error setting up tracing",
			slog.Any("error", err),
		)
	}

	return s, nil
}

func (s *Sauté) Handle(err error) {
	s.Logger.Error("telemetry error",
		slog.Any("error", err),
	)
}

func (s *Sauté) SetErrorHandler(config *Config, 𝑓 func(otel.ErrorHandler)) {
	switch {
	case config.ErrorHandler == nil:
		// Not set → use default
		𝑓(s)

	case *config.ErrorHandler == nil:
		// Explicit nil → do nothing

	default:
		// Set → use that
		𝑓(*config.ErrorHandler)
	}
}

func (s *Sauté) Stop(ctx context.Context) {
	slices.Reverse(s.shutdowners)

	for _, shutdowner := range s.shutdowners {
		if err := shutdowner.Shutdown(ctx); err != nil {
			s.ErrorContext(ctx, "failed to shut down "+shutdowner.string,
				slog.Any("error", err),
			)
		}
	}

	s.shutdowners = nil
}

func (s *Sauté) metrics(ctx context.Context, config *Config, res *resource.Resource) error {
	metricReader, err := autoexport.NewMetricReader(ctx)
	if err != nil {
		return fmt.Errorf("failed to create metric reader: %w", err)
	}

	meterProvider := metric.NewMeterProvider(
		metric.WithResource(res),
		metric.WithReader(metricReader),
	)
	s.shutdowners = append(s.shutdowners, NamedShutdowner{"meter provider", meterProvider})

	otel.SetMeterProvider(meterProvider)

	if config.Host {
		go func() {
			if err := host.Start(); err != nil {
				s.ErrorContext(ctx, "failed to start host metrics",
					slog.Any("error", err),
				)
			}
		}()
	}

	if config.Runtime {
		go func() {
			if err := runtime.Start(); err != nil {
				s.ErrorContext(ctx, "failed to start runtime metrics",
					slog.Any("error", err),
				)
			}
		}()
	}

	return nil
}

func (s *Sauté) resource(ctx context.Context, config *Config) (res *resource.Resource, err error) {
	detected, err := resource.Detect(ctx)
	if err != nil {
		s.ErrorContext(ctx, "error while detecting resources",
			slog.Any("error", err),
		)
	}

	for _, r := range [...]*resource.Resource{
		resource.Default(),
		resource.Environment(),
		detected,
		config.Resource,
		resource.NewWithAttributes(
			resource.Default().SchemaURL(),
			config.Attributes...,
		),
	} {
		switch {
		case res == nil:
			res = r
			fallthrough
		case r == nil:
			continue
		}

		if res, err = resource.Merge(res, r); err != nil {
			return nil, fmt.Errorf("failed to merge resources: %w", err)
		}
	}

	return
}

func (s *Sauté) tracing(ctx context.Context, config *Config, res *resource.Resource) error {
	otel.SetTextMapPropagator(config.TextMapPropagator)

	exporter, err := autoexport.NewSpanExporter(ctx)
	if err != nil {
		return fmt.Errorf("failed to create span exporter: %w", err)
	}

	tracingSpanProcessor := trace.NewBatchSpanProcessor(exporter)
	s.shutdowners = append(
		s.shutdowners,
		NamedShutdowner{"tracing span processor", tracingSpanProcessor},
	)

	tracingProvider := trace.NewTracerProvider(
		trace.WithResource(res),
		trace.WithSpanProcessor(tracingSpanProcessor),
	)
	s.shutdowners = append(s.shutdowners, NamedShutdowner{"tracing provider", tracingProvider})

	otel.SetTracerProvider(tracingProvider)

	return nil
}

type Shutdowner interface {
	Shutdown(context.Context) error
}
