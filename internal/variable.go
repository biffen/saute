//nolint:revive,stylecheck
package internal

import (
	"context"
	"log/slog"
	"strings"
)

var (
	_ Variable[string] = Const[string]{""}
	_ Variable[string] = (*EnvVar[string])(nil)

	OTEL_LOG_LEVEL = EnvVar[slog.Level]{
		Parent: Const[slog.Level]{slog.LevelInfo},
		Key:    "OTEL_LOG_LEVEL",
	}

	OTEL_SDK_DISABLED = EnvVar[bool]{
		Parent: Const[bool]{false},
		Key:    "OTEL_SDK_DISABLED",
	}
)

type Const[T any] struct {
	Value T
}

func (d Const[T]) Get(context.Context, func(string) string) (T, error) {
	return d.Value, nil
}

type EnvVar[T any] struct {
	Parent Variable[T]
	Key    string
}

func (v *EnvVar[T]) Get(
	ctx context.Context,
	getenv func(string) string,
) (t T, err error) {
	str := getenv(v.Key)
	if str == "" {
		if v.Parent != nil {
			t, err = v.Parent.Get(ctx, getenv)
		}
		return
	}

	switch u := any(&t).(type) {
	case *bool:
		switch strings.ToLower(str) {
		case "", "false":
			*u = false

		case "true":
			*u = true

		default:
			slog.WarnContext(ctx, "bad boolean value, using false",
				slog.String("key", v.Key),
				slog.String("value", str),
			)
		}

	case *slog.Level:
		tmp, err := ParseLogLevel(ctx, str)
		if err == nil {
			*u = tmp
		} else {
			slog.WarnContext(ctx, "failed to parse log level, using "+u.String(),
				slog.Any("error", err),
				slog.String("key", v.Key),
				slog.String("value", str),
			)
		}

	default:
		panic(0)
	}

	return
}

type Variable[T any] interface {
	Get(context.Context, func(string) string) (T, error)
}
