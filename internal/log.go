package internal

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"math"
	"strings"
)

const (
	LevelAll  slog.Level = math.MinInt
	LevelNone slog.Level = math.MaxInt
)

var (
	ErrBadLogLevel              = errors.New("bad log level")
	_              slog.Handler = (*Handler)(nil)
)

func ParseLogLevel(_ context.Context, str string) (level slog.Level, err error) {
	if err = level.UnmarshalText([]byte(str)); err == nil {
		return level, nil
	}

	var ok bool
	if level, ok = map[string]slog.Level{
		"":              slog.LevelInfo,
		"alert":         slog.LevelError,
		"all":           LevelAll,
		"crit":          slog.LevelError,
		"critical":      slog.LevelError,
		"dbg":           slog.LevelDebug,
		"emerg":         slog.LevelError,
		"emergency":     slog.LevelError,
		"err":           slog.LevelError,
		"fatal":         slog.LevelError,
		"information":   slog.LevelInfo,
		"informational": slog.LevelInfo,
		"none":          LevelNone,
		"notice":        slog.LevelInfo,
		"panic":         slog.LevelError,
		"verbose":       LevelAll,
		"warning":       slog.LevelWarn,
	}[strings.ToLower(str)]; ok {
		return level, nil
	}

	return 0, fmt.Errorf("%w %q", ErrBadLogLevel, str)
}

type Handler struct {
	Level slog.Level
	Next  slog.Handler
}

func (h *Handler) Enabled(ctx context.Context, level slog.Level) bool {
	return h.Next != nil && level >= h.Level && h.Next.Enabled(ctx, level)
}

func (h *Handler) Handle(ctx context.Context, record slog.Record) error {
	//nolint:wrapcheck // Nah.
	return h.Next.Handle(ctx, record)
}

func (h *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return h.clone(func(h slog.Handler) slog.Handler {
		return h.WithAttrs(attrs)
	})
}

func (h *Handler) WithGroup(name string) slog.Handler {
	return h.clone(func(h slog.Handler) slog.Handler {
		return h.WithGroup(name)
	})
}

func (h *Handler) clone(𝑓 func(slog.Handler) slog.Handler) slog.Handler {
	if h.Next == nil {
		return h
	}

	return &Handler{
		Level: h.Level,
		Next:  𝑓(h.Next),
	}
}
