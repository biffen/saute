package internal

import (
	"runtime"
	"strings"

	"go.opentelemetry.io/otel/attribute"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
)

func Code(skip int) (attrs []attribute.KeyValue, function string) {
	pc, file, line, ok := runtime.Caller(skip + 1)
	if !ok {
		return
	}

	const maxCodeAttributes = 4
	attrs = make([]attribute.KeyValue, 0, maxCodeAttributes)

	if fun := runtime.FuncForPC(pc); fun != nil && fun.Name() != "" {
		function = fun.Name()

		if delim := strings.LastIndexByte(function, byte('.')); delim > 0 {
			attrs = append(attrs,
				semconv.CodeFunction(function[delim+1:]),
				semconv.CodeNamespace(function[:delim]),
			)
		} else {
			attrs = append(attrs,
				semconv.CodeFunction(function),
			)
		}
	}

	if file != "" {
		attrs = append(attrs, semconv.CodeFilepath(file))
	}

	if line > 0 {
		attrs = append(attrs, semconv.CodeLineNumber(line))
	}

	return
}
