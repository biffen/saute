package sauté

import (
	"gitlab.com/biffen/saute/internal"
	"go.opentelemetry.io/otel/attribute"
)

// CodeAttributes returns ‘code.*’ attributes for the caller.
func CodeAttributes() (attrs []attribute.KeyValue) {
	attrs, _ = internal.Code(1)
	return
}
