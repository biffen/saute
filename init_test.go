package sauté_test

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"testing"

	"github.com/dotse/slug"
	"github.com/stretchr/testify/assert"
	sauté "gitlab.com/biffen/saute"
)

func ExampleInit() {
	// Set up a context.Context
	ctx, stop := signal.NotifyContext(
		context.Background(),
		os.Interrupt,
		syscall.SIGTERM,
	)
	defer stop()

	// Set up logging
	slog.SetDefault(slog.New(slug.NewHandler(slug.HandlerOptions{
		HandlerOptions: slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}, os.Stderr)))

	// Now start telemetry!
	fini, err := sauté.Init(ctx,
		sauté.LogLevel(slog.LevelDebug),
	)
	// Optionally handle a serious error
	if err != nil {
		panic(err)
	}

	// Don’t forget to stop telemetry
	defer fini()

	// Output:
}

//nolint:paralleltest // Can’t.
func TestInit(t *testing.T) {
	var (
		ctx = context.Background()
		𝑓   = func(t *testing.T, name string, options ...sauté.Option) {
			t.Helper()

			t.Run(name, func(t *testing.T) {
				t.Helper()

				fini, err := sauté.Init(ctx, options...)
				assert.NotNil(t, fini, "always a func")
				assert.NoError(t, err, "error from Init()")
				fini()
			})
		}
	)

	𝑓(t, "no options")
	𝑓(t, "nil logger", sauté.Logger(nil))
}
