package sauté

import (
	"context"
	"log/slog"

	"gitlab.com/biffen/saute/internal"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
)

type Option interface {
	internal.Option
}

// Attributes adds attributes to all telemetry.
func Attributes(attributes ...attribute.KeyValue) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Attributes = append(config.Attributes, attributes...)
			return nil
		},
	)
}

// Detectors adds [go.opentelemetry.io/otel/sdk/resource.Detector]:s for
// resources.
func Detectors(detectors ...resource.Detector) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Detecors = append(config.Detecors, detectors...)
			return nil
		},
	)
}

// ErrorHandler specifies an [go.opentelemetry.io/otel.ErrorHandler] to set. The
// default is to log the error (see also: [Logger]).
//
// Using this function with a nil argument sets a no-op error handler.
//
// See also: [go.opentelemetry.io/otel.SetErrorHandler].
func ErrorHandler(handler otel.ErrorHandler) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.ErrorHandler = &handler
			return nil
		},
	)
}

// Getenv sets the function to use for getting environment variables. The
// default is [os.Getenv].
func Getenv(getenv func(string) string) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Getenv = getenv
			return nil
		},
	)
}

// Host sets whether to use [go.opentelemetry.io/contrib/instrumentation/host]
// for host metrics. The default is true.
func Host(host bool) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Host = host
			return nil
		},
	)
}

// LogLevel sets the log level of the default logger. If [Logger] is used then
// this does nothing. The default is parsed from OTEL_LOG_LEVEL.
func LogLevel(level slog.Leveler) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			tmp := level.Level()
			config.LogLevel = &tmp
			return nil
		},
	)
}

// Logger sets a logger for telemetry. Default is [log/slog]’s default with a
// sybsystem=telemetry attribute.
func Logger(logger *slog.Logger) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Logger = logger
			return nil
		},
	)
}

// Resource sets a custom [go.opentelemetry.io/otel/sdk/resource.Resource] to
// merge with the others.
func Resource(res *resource.Resource) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Resource = res
			return nil
		},
	)
}

// Runtime sets whether to use
// [go.opentelemetry.io/contrib/instrumentation/runtime] for runtime metrics.
// The default is true.
func Runtime(runtime bool) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.Runtime = runtime
			return nil
		},
	)
}

// TextMapPropagator sets a
// [go.opentelemetry.io/otel/propagation.TextMapPropagator]. The default is to
// use [go.opentelemetry.io/contrib/propagators/autoprop.NewTextMapPropagator].
func TextMapPropagator(propagator propagation.TextMapPropagator) Option {
	return internal.OptionFunc(
		func(_ context.Context, config *internal.Config) error {
			config.TextMapPropagator = propagator
			return nil
		},
	)
}
